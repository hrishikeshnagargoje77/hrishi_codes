//Header files
#include <stdio.h>

void Pattern(int n) {
  int t=1;
  int i,p;
  p=n;
  while(n != 0) {
    if(t % 2 != 0){
      for(i=1; i<=n; i++) {
        printf("%d ",i);
      }
    } 
    if(t % 2 == 0){
      for(i=n; i>=1; i--) {
        printf("%d ",i);
      }
    }
    printf("\n");
    if(t % 2 != 0){
      for(i=1; i<p;i++) {
        printf("  ");
      }
    } 
    if(t % 2 == 0){
      for(i=1; i<t+1;i++) {
        printf("  ");
      }
    }
    
    n--;
    t++;
  }
}


int main()
{ 
  int N=0;

  printf("Enter the N : ");
  scanf("%d",&N);

  Pattern(N);
  
  // EXIT_SUCCESS,
  return 0;
}