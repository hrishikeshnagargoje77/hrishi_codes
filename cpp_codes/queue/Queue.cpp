#include<iostream>
using namespace std;

class Queue {
public:
  int *queue;
  int capacity;
  int front;
  int rear;
  int size; 

  //costructor
  Queue(int size);
  int dequeue();
  void enqueue(int x);
  bool isEmpty();
  bool isFull();
  void printQueue();
};

Queue::Queue(int size) {
  queue = new int[size];
  capacity = size;
  front = -1;
  rear = -1;
}

int Queue::dequeue() {
  int result;
  printQueue ();
  
  if (isEmpty()) {
    cout << "Cannot dequeue anything. Queue underflow\n";
    return 0;
  }

  result = queue[front];
  cout << "\tDequeueing " << result << "\n";
  if (front >= rear) {
    front = -1;
    rear = -1;
  }
  else {
    front++;
  }
  
  return result;
}

void Queue::enqueue(int num) {
  printQueue ();
  if (isFull()) {
    cout << "Cannot enqueue " << num << ". Queue is full\n";
    return;
  }
  cout << "\tEnqueueing " << num << "\n";
  
  if (isEmpty()) {
    front = -1;
    rear = -1;
  }

   if (front == -1) front = 0;
      rear++;
      queue[rear] = num;
}

bool Queue::isEmpty() {
  if (front == -1)
      return true;
    else
      return false;
}

bool Queue::isFull() {
  if (front == 0 && rear == size - 1) {
      return true;
    }
    return false;
}

void Queue::printQueue() {
  cout << "Queue contents are ";
  for (int i=front; i <=rear; i++) {
      cout << queue[i] << " ";
  }
  cout << "\n";
}


int main()
{
  Queue QObj(0);

  QObj.enqueue(7);
  QObj.enqueue(5);
  QObj.enqueue(3);
  QObj.dequeue();
  QObj.dequeue();
  QObj.enqueue(4);
  QObj.enqueue(6);
  QObj.dequeue();
  QObj.dequeue();
  QObj.dequeue();

  return 0;
} //end of the main program
