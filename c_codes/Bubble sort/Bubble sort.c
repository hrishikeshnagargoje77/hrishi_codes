#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 10

void PrintPointerArray(int *arr, size_t length) {
  for(int i=0; i<length; i++) {
   printf("%d ",arr[i]); 
  }
}

void SortArray(int *a, size_t n) {
  int i, j, temp;  
  for(i = 0; i < n; i++)  {    
    for(j = i+1; j < n; j++) {    
      if(a[j] < a[i]) {    
        temp = a[i];    
        a[i] = a[j];    
        a[j] = temp;     
      }     
    }     
  }     
}

void PrintArray(int *arr, size_t length) {
  for(int i=0; i<length; i++) {
   printf("%d ",arr[i]); 
  }
}

void FillArray(int *arr, size_t length) 
{
  srand(time(NULL));
  for(int i=0; i<length; i++) {
    int random = rand() % 101; //range[0,100]
    arr[i] = random;
  }
}

int main()
{
  int Numbers[SIZE];
  int *PtrArr = Numbers;
  printf("Generating List...\n");
  FillArray(PtrArr, SIZE);
  printf("\nRandom List Generated: ");
  PrintArray(PtrArr, SIZE);
  
  SortArray(PtrArr,SIZE);
  printf("\nSort List Generated\t : ");
  PrintPointerArray(PtrArr, SIZE);
  printf("\n");
  return 0;
}