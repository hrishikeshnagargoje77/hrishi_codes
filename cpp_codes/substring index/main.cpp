//Index of the start point of certain sub string From given string
#include <iostream>
#include <string>
using namespace std;

int returnIndex(string str, string subStr) {
  int index=0;
  index = str.find(subStr);
  
  return index;
}

int main()
{
  string input1,input2;
  int index;

  // Reads inputs
  cout << "Please enter first line input string : ";
  cin >> input1;
  cout << "Please enter second line input sub-string : ";
  cin >> input2;

  index = returnIndex(input1,input2);
  if(index < 0){
    cout << "wrong sub-String" << endl;
  } else {
    cout << "Index : " << index << endl;
  }
  
  // EXIT_SUCCESS
  return 0;
}